from fastapi import APIRouter, FastAPI
import joblib
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

app = FastAPI()

router = APIRouter(prefix="/predic")
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Allow requests from any origin
    allow_methods=["*"],  # Allow all HTTP methods
    allow_headers=["*"],  # Allow all headers
)

class Bodyfat(BaseModel):
    Age: int
    Weight: float
    Height: float
    Neck: float
    Wrist: float

# Define a default model
model = None

# Load the model using joblib
try:
    model = joblib.load('bodyfat.pkl')
    print("Model loaded successfully.")
    print(model)
except FileNotFoundError:
    print("Error: Model file 'bodyfat.pkl' not found.")
except Exception as e:
    print("Error loading model:", e)

# Check if the model is loaded successfully
if model is not None:
    def predict(bodyfat: Bodyfat):
        # Make predictions
        predictions = model.predict([[bodyfat.Age, bodyfat.Weight, bodyfat.Height, bodyfat.Neck, bodyfat.Wrist]])
        return {"input": bodyfat.dict(), "prediction": predictions.tolist()}

    # Define a GET endpoint to predict bodyfat
    @router.get("/")
    def predict_bodyfat(Age: int, Weight: float, Height: float, Neck: float, Wrist: float):
        bodyfat_data = Bodyfat(Age=Age, Weight=Weight, Height=Height, Neck=Neck, Wrist=Wrist)
        prediction_result = predict(bodyfat_data)
        return prediction_result['prediction'][0]


    # Define a POST endpoint to create bodyfat prediction
    @router.post("/")
    def create_bodyfat_prediction(bodyfat: Bodyfat):
        prediction = predict(bodyfat)
        return prediction

else:
    # Define a GET endpoint when the model is not loaded
    @router.get("/")
    def predict_bodyfat(input: float):
        return {"input": input, "prediction": "Error: Model not loaded."}

# Include the router in the FastAPI app
app.include_router(router)
