from fastapi import APIRouter
from schemas.User import User

router = APIRouter(prefix="/users")

last_id = 1
users = [
    User(id = 1, email = "user1@mail.com", name = "user1", password = "Pass@1234", age = 20)
]

#Post()
@router.post("/") # Creat
def creat_user(user: User):
    global last_id
    last_id =last_id + 1
    user.id = last_id + 1
    users.routerend(user)
    return user

#Get()
@router.get("/")
def get_users():
    return users

#Get(id)
@router.get("/{user_id}")
def get_user(user_id: int):
    for user in users:
        if user.id == user_id:
            return user
    return None

#Update()
@router.put("/{user_id}")
def update_user(user_id: int, user: User):
    for index, u in enumerate(users):
        if u.id == user_id:
            users[index] = user
            return user
    return None

#Del()
@router.delete("/{user_id}")
def delete_user(user_id: int):
    for user in users:
        if user.id == user_id:
            users.remove(user)
            return user
    return None