from fastapi import APIRouter

router = APIRouter(prefix="/celcius")


@router.get("/{celcius}farenheit")
def cal_farenheit(celcius: float):
    return {
        "celcius": celcius,
        "farenheit": celcius * 9 / 5 + 32
        }

@router.get("/")
def cal_farenheit(c: float):
    return {
        "celcius": c,
        "farenheit": c * 9 / 5 + 32
        }