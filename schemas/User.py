from pydantic import BaseModel

class User(BaseModel):
    id: int|None
    email: str
    name: str
    password: str
    age: int