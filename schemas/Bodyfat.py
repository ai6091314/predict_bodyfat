from pydantic import BaseModel

class Bodyfat(BaseModel):
    Age: int
    Weight: int
    Height: int
    Neck: int
    Wrist: int