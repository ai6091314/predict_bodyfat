# library
#from typing import Union
from fastapi import FastAPI
#from pydantic import BaseModel

# router
import routes.celcius as celcius
import routes.users as users
#import routes.predict_bodyfat as predic

app = FastAPI()
app.include_router(celcius.router)
app.include_router(users.router)
#Sapp.include_router(predic.router)

@app.get("/")
def read_root():
    return {"Hello": "World"} #Json

# query String
# http://127.0.0.1:8000/items/1?q=charudet&r=1

#Get()
@app.get("/items/{item_id}")
def read_item(item_id: int, q: str| None = None, r: int| None = None):
    return {"item_id": item_id, "q": q, "r": r}
